<?php 
class UserModel extends Model{
	protected $_validate=array(
		array('verify','require','验证码不能为空'),
		array('verify','checkVerify','验证码错误',0,'callback',1),

		array('username','require','用户名不能为空'),
		array('username','','用户已存在',0,'unique',1),

		array('password','require','密码不能为空'),
		array('password','password2','两次密码不一致',0,'confirm'),

		
		
	);

	protected function checkVerify($verify){
		if(md5($verify)!=$_SESSION['verify']){
			return false;
		}else{
			return true;
		}
	}
}
?>
