<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>注册</title>
	<!--<link rel='stylesheet' type='text/css' href="__PUBLIC__/Css/Home/login.css">-->
	<link rel='stylesheet' type='text/css' href="__PUBLIC__/Css/bootstrap.css">
	<link rel='stylesheet' type='text/css' href="__PUBLIC__/Css/Home/login.css">
	<script type="text/javascript" src="__PUBLIC__/Js/jquery.js"></script>
	<script>
		var error=new Array();

		$(function(){
			$('input[name="username"]').blur(function(){
				var username=$(this).val();
				$.get('__URL__/checkName',{'username1':username},function(data){
					if(data==0){
						error['username']=1;
							$('input[name="username"]').after('<p id="umessage" style="color:red">该用户已注册</p>');

					}else{
						error['username']=0;
						$('#umessage').remove();
					}
				});
			})		
		})

		$(function(){
				$("img[class='register']").click(function(){
					if(error['username']==0)
					$("form[name='myForm']").submit();	
				});	
				$('img[class="register"]').click(function(){
					$('form[name="myForm"]').reset();	
				})
		})

	</script>
	<style>
		h2{
			margin-left:auto;
			margin-right:auto;
		}
	</style>
</head>
<body>
<div class='container'>
	<h2>
		<center class='form-signin-heading'>注册</center>
	</h2>
	<form class='form-signin' role='form' action="__APP__/Register/doReg" method='post' name='myForm'>
			<div class='control-grokddup'>
				<label for="inputUsername">用 户 名：</label>
					<input  class="form-control" id='inputUsername' type='text' name='username'/>
			</div>

			<div class='control-group'>
				<label  for='inputPwd'>密　　码：</label>
					<input  class='form-control' id='inputPwd' type='password' name='password'/>
			</div>

			<div class='control-group'>
				<label  for='inputPwd2'>确认密码： </label>
					<input class='form-control' id='inputPwd2' type='password' name='password2'/>
			</div>

			<label >性　　别：</label>
				<label class='radio-inline'>
					<input type="radio" name='sex' value='1' />男
				</label>
				<label class='radio-inline'>
					<input type='radio' name='sex' value='0' />女
				</label>

			<div class='form-group'>
				<label for='inputVerify'>验 证 码：</label>
				<input id='inputVerify' class='form-control' type='text' name='verify'/>
				<img src='__APP__/Public/verify?w=80&h=40' onclick='this.src=this.src+"?"+Math.random()' ><br/>
			</div>

			<center>
				<button type='submit' class='btn btn-primary'>注册</button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type='reset' class='btn btn-difault'>重置</button>
			</center>
	</form>
</div>
</html>