<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>right</title>
	<link rel="stylesheet" href="__PUBLIC__/Css/bootstrap.css">
	<link rel="stylesheet" href="__PUBLIC__/Css/Home/index.css">
</head>
<body>
<div id='main' class='container'>
	<form class='form-horizontal' role='form' action="__APP__/Message/doMess" method='post' enctype='multipart/form-data'>
		<div class='form-group'>
			<label class='control-label'>留言题目:</label>
			<input class='form-control' type="text" name='title' />
		</div>
		<br>
		
		<div class='form-group'>
			<label class='control-label'>留言内容:</label>
			<textarea class='form-control' name="content" cols="30" rows="10"></textarea>
		</div>
		<br/>
		<label class='control-label'>附　　件:</label>
		<input class='form-control' type="file" name='filename'> <br>
		<button type="submit" class='btn btn-primary'>提交</button>
	</form>
</div>
</body>
</html>